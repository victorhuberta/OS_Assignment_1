/**
 * Date: 14th May 2016
 * Header: main; @Author: Victor Huberta
 * ``````````````````````````````````````
 * Include all C libraries needed.
 * ``````````````````````````````````````
 *
 */

#ifndef MAIN_H
#define MAIN_H

#if __STDC_VERSION__ >= 199901L
#define _XOPEN_SOURCE 600
#else
#define _XOPEN_SOURCE 500
#endif /* __STDC_VERSION__ */

#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>

/**
 * Remove link pipe FIFO file before exit.
 */
void delete_link_pipe();

#endif
