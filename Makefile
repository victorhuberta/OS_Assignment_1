CC=gcc
CFLAGS=-ansi -pedantic -Wall -lrt

MAIN=main/main.c
DEPS=pipes/pipes.c messages/messages.c branches/branches.c
OUTPUT=parent

all:
	$(CC) $(CFLAGS) $(MAIN) $(DEPS) -o $(OUTPUT)

clean:
	rm $(OUTPUT) *.log
