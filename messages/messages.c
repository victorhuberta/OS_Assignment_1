/**
 * Date: 14th May 2016
 * Module: messages; @Author: Victor Huberta
 * `````````````````````````````````````````````````
 * This module handles how messages are read
 * and written by the processes. It also handles
 * how every message passing is logged into a file.
 * It specifies the format of messages and logs,
 * as well as which input or output streams to use.
 * `````````````````````````````````````````````````
 */

#include "../main/main.h"
#include "../branches/branches.h"
#include "messages.h"

/**
 * Function: read_file_and_handle_messages
 *
 * args: void.
 * `````````````````````````````````````````````
 * Read all messages from file and pass them
 * to child processes. After the final message
 * has been passed, send a END_MSG to eventually
 * end the cycle.
 * `````````````````````````````````````````````
 * returns: void.
 *
 */
void read_file_and_handle_messages() {
    char msg[MAX_MSG_SIZE] = {0};

    FILE *file = fopen(FILE_NAME, "r");
    if (file == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    while (fgets(msg, MAX_MSG_SIZE, file) != NULL)
        handle_message(0, msg);

    if (SEND_END_MSG() == -1) {
        perror("write");
        exit(EXIT_FAILURE);
    }
    fclose(file);
}

/**
 * Function: wait_and_handle_messages
 *
 * args:
 * index -> a process' index in process list.
 * ``````````````````````````````````````````
 * Wait for new messages passed from STDIN,
 * and handle them accordingly.
 * ``````````````````````````````````````````
 * returns: void.
 *
 */
void wait_and_handle_messages(int index) {
    char msg[MAX_MSG_SIZE] = {0};

    while (fgets(msg, MAX_MSG_SIZE, stdin) != NULL) {

        if (THIS_IS_THE_END(msg)) {
            /* Parent doesn't need to continue sending */
            if (! IS_PARENT(index)) {
                if (SEND_END_MSG() == -1) {
                    perror("write");
                    exit(EXIT_FAILURE);
                }
            }
            break;
        }

        if (IS_PARENT(index)) {
            LOG_ACTION_TO_SCREEN("MESSAGE DISCARDED.");
            log_traffic(index, msg, DISCARD);
        }
        else handle_message(index, msg);
    }
}

/**
 * Function: handle_message
 *
 * args:
 * index -> a process' index in process list.
 * msg -> the received message.
 * ```````````````````````````````````````````````
 * If END_MSG has not yet been received, check
 * whether or not the message is for this process.
 * If it is, log event and keep the message. Else,
 * forward the message to STDOUT and log event.
 * ```````````````````````````````````````````````
 * returns: void.
 *
 */
void handle_message(int index, char msg[]) {
    int proc_no; sscanf(msg, "%d", &proc_no);

    if (MSG_IS_FOR_CURRENT_PROCESS(proc_no, index)) {

        LOG_ACTION_TO_SCREEN("MESSAGE KEPT.");
        log_traffic(index, msg, KEEP);

    } else {
        LOG_ACTION_TO_SCREEN("MESSAGE FORWARDED.");

        if (write(STDOUT_FILENO, msg, strlen(msg)) == -1) {
            perror("write");
            exit(EXIT_FAILURE);
        }

        log_traffic(index, msg, FORWARD);
    }
}

/**
 * Function: log_traffic
 *
 * args:
 * index -> a process' index in process list.
 * msg -> the received message.
 * action -> KEEP, FORWARD, or DISCARD.
 * `````````````````````````````````````````````
 * Log a new event to a file with the following
 * format:
 * <timestamp>[tab]<message>[tab]<action>
 * `````````````````````````````````````````````
 * returns: void.
 *
 */
void log_traffic(int index, char msg[], int action) {
    char timestr[TIMESTR_LEN] = {0};
    char action_str[8] = {0};
    char log_file_name[14] = {0};

    get_timestr(timestr);

    prepare_for_logging(action_str, log_file_name, index, msg, action);
    log_to_file(timestr, action_str, log_file_name, msg);
}

/**
 * Function: get_timestr
 *
 * args:
 * timestr -> address which the full timestamp will be written at.
 * ````````````````````````````````````````````````````````````````
 * Get a full timestamp string by appending nanoseconds to the
 * date and time received from ctime(1).
 * ````````````````````````````````````````````````````````````````
 * returns: void.
 *
 */
void get_timestr(char timestr[]) {
    char *timestamp;
    time_t curtime;
    long nanos = get_timestamp_nanos();

    time(&curtime);
    timestamp = ctime(&curtime);

    TRIM_NEWLINE(timestamp);

    sprintf(timestr, "%s.%ld", timestamp, nanos);
}

long get_timestamp_nanos() {
    struct timespec ts;
    
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
        perror("clock_gettime");
        exit(EXIT_FAILURE);
    }

    return (long) ts.tv_nsec % 1000000000L;
}

/**
 * Function: prepare_for_logging
 *
 * args:
 * action_str -> address which either "KEEP", "FORWARD", or
 *   "DISCARD" will be written at.
 * log_file_name -> the log file's name.
 * `````````````````````````````````````````````````````````
 * Prepare all variables needed for logging i.e. action
 * string, log file's name (taken from process index), and
 * the actual message.
 * `````````````````````````````````````````````````````````
 * returns: void.
 *
 */
void prepare_for_logging(char action_str[], char log_file_name[],
        int index, char msg[], int action) {

    strcpy(action_str, (action == KEEP) ? "KEEP" :
        ((action == FORWARD) ? "FORWARD" : "DISCARD"));

    snprintf(log_file_name, 14, LOG_NAME_FORMAT, index);
    TRIM_NEWLINE(msg);
}

void log_to_file(char timestr[], char action_str[],
        char log_file_name[], char msg[]) {

    FILE *log_file = fopen(log_file_name, "a");
    if (log_file == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    fprintf(log_file, LOG_FORMAT, timestr, msg, action_str);
    fclose(log_file);
}
